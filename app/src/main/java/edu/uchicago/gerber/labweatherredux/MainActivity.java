package edu.uchicago.gerber.labweatherredux;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.google.gson.Gson;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.util.ArrayList;
import java.util.List;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;

import edu.uchicago.gerber.labweatherredux.jigs.Response;
import edu.uchicago.gerber.labweatherredux.jigs.Temp;

public class MainActivity extends AppCompatActivity {


    //use this string to get the sample json
    //https://api.openweathermap.org/data/2.5/forecast/daily?q=boston&units=imperial&cnt=16&APPID=acb056481e1dddbd0324f1ca9d349f4f

//use this to generate the jigs, and use Response for classname, and the full package to your jigs: e.g. edu.uchicago.gerber.labweatherredux.jigs for package.

    //http://www.jsonschema2pojo.org/

    // List of Weather objects representing the forecast
    private List<Weather> weatherList = new ArrayList<>();

    private ImageView imageCity;

    // ArrayAdapter for binding Weather objects to a ListView
    private WeatherArrayAdapter weatherArrayAdapter;
    private ListView weatherListView; // displays weather info
    private  EditText locationEditText;
    private FloatingActionButton fab;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        imageCity = (ImageView) findViewById(R.id.imageCity);



        // create ArrayAdapter to bind weatherList to the weatherListView
        weatherListView = (ListView) findViewById(R.id.weatherListView);
        weatherArrayAdapter = new WeatherArrayAdapter(this, weatherList);
        weatherListView.setAdapter(weatherArrayAdapter);

        locationEditText  = findViewById(R.id.locationEditText);
        locationEditText.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {

                if (keyCode == KeyEvent.KEYCODE_ENTER){
                    fireUrl(fab);
                    return true;
                }
                return false;
            }
        });


        fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                fireUrl(view);



            }
        });
    }

    private void fireUrl(View view) {
        URL url = createURL(locationEditText.getText().toString());

        // hide keyboard and initiate a GetWeatherTask to download
        // weather data from OpenWeatherMap.org in a separate thread
        if (url != null) {
            dismissKeyboard(locationEditText);
            GetWeatherTask getLocalWeatherTask = new GetWeatherTask();
            getLocalWeatherTask.execute(url);
        }
        else {

            //this may need to be in a catch block
            Snackbar.make(view, R.string.invalid_url, Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show();

        }
    }


    // programmatically dismiss keyboard when user touches FAB
    private void dismissKeyboard(View view) {
        InputMethodManager imm = (InputMethodManager) getSystemService(
                Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    // create openweathermap.org web service URL using city
    private URL createURL(String city) {
        String apiKey = getString(R.string.api_key);
        String baseUrl = getString(R.string.web_service_url);

        try {
            // create URL for specified city and imperial units (Fahrenheit)
            String urlString = baseUrl + URLEncoder.encode(city, "UTF-8") +
                    "&units=imperial&cnt=16&APPID=" + apiKey;
            return new URL(urlString);
        }
        catch (Exception e) {
            e.printStackTrace();
        }

        return null; // URL was malformed
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    // makes the REST web service call to get weather data and
    // saves the data to a local HTML file
    private class GetWeatherTask
            extends AsyncTask<URL, Void, JSONObject> {

        @Override
        protected JSONObject doInBackground(URL... params) {


            JSONObject object= null;

            try {
              object =  Util.getJSON(params[0].toString());
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return object;
        }


        // process JSON response and update ListView
        @Override
        protected void onPostExecute(JSONObject weather) {
            if (weather == null) {
                Toast.makeText(MainActivity.this, "API did not reply", Toast.LENGTH_SHORT).show();
                return;
            }

            Response response =  convertJSONtoArrayList(weather); // repopulate weatherList
            weatherArrayAdapter.notifyDataSetChanged(); // rebind to ListView
            weatherListView.smoothScrollToPosition(0); // scroll to top


            Glide.with(MainActivity.this).load("http://maps.google.com/maps/api/staticmap?center=" + response.getCity().getCoord().getLat()+ "," + response.getCity().getCoord().getLon() +
                    "&zoom=10&size=800x800&sensor=false").asBitmap().listener(new RequestListener<String, Bitmap>() {
                @Override
                public boolean onException(Exception e, String model, Target<Bitmap> target, boolean isFirstResource) {
                    e.printStackTrace();
                    return false;
                }

                @Override
                public boolean onResourceReady(Bitmap resource, String model, Target<Bitmap> target, boolean isFromMemoryCache, boolean isFirstResource) {
                    //if(!isFromMemoryCache) marker.showInfoWindow();
                    return false;
                }
            }).into(imageCity);
        }
    }


    // create Weather objects from JSONObject containing the forecast
    private Response convertJSONtoArrayList(JSONObject forecast) {
        weatherList.clear(); // clear old weather data



        Gson gson = new Gson();

        Response response =  gson.fromJson(forecast.toString(), Response.class);

        Log.d("RESP", response.toString());

        float lat = response.getCity().getCoord().getLat().floatValue();
        float lon =  response.getCity().getCoord().getLon().floatValue();

        // http://maps.googleapis.com/maps/api/streetview?size=400x400&location=40.720032,%20-73.988354&fov=90&heading=235&pitch=10&sensor=false
        String url = "http://maps.google.com/maps/api/staticmap?center=" + lat + "," + lon + "&zoom=15&size=200x200&sensor=false";





        List<edu.uchicago.gerber.labweatherredux.jigs.List> list = response.getList();

        for (int i = 0; i <list.size(); i++) {
            edu.uchicago.gerber.labweatherredux.jigs.List day =    list.get(i);
            Temp temp = day.getTemp();
            List<edu.uchicago.gerber.labweatherredux.jigs.Weather> weathers = day.getWeather();
            edu.uchicago.gerber.labweatherredux.jigs.Weather weather = weathers.get(0);

            weatherList.add(new Weather(


                    day.getDt(), // date/time timestamp
                    temp.getMin(), // minimum temperature
                    temp.getMax(), // maximum temperature
                    day.getHumidity(), // percent humidity
                    weather.getDescription(), // weather conditions
                    weather.getIcon(),
                    response.getCity().getCoord()

            ));



        }

           return  response;

    }
}
